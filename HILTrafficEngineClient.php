<?php

class HILTrafficEngineClient
{
	const SIDEBAR_IMAGE_TYPE = 'square';
	const MAINBANNER_IMAGE_TYPE = 'hd banner';

	const OFFER_ZIPCODE = 'zipcode';
	const OFFER_VERTICAL = 'vertical';
	const OFFER_TYPE = 'type';
	const OFFER_IMAGE = 'image';
	const OFFER_META_COUNT = 'count';
	const OFFER_META = 'meta';
	const OFFER_OFFER = 'offer';
	const OFFER_CUSTOM = 'custom';

	const TRAFFICENGINE_URL = 'http://trafficengine.hilprod.com/offers/retrieve/';


	public function __construct($apikey)
	{
		$this->SetAPIKey($apikey);
	}


	public function GetSidebarOffers($zipcode, $vertical, $count, $custom_data)
	{
		return $this->GetOffers($zipcode, $vertical, $count, $custom_data, self::SIDEBAR_IMAGE_TYPE);
	}


	public function GetMainBannerOffers($zipcode, $vertical, $count, $custom_data)
	{
		return $this->GetOffers($zipcode, $vertical, $count, $custom_data, self::MAINBANNER_IMAGE_TYPE);
	}


	private function GetOffers($zipcode, $vertical, $count, $custom_data, $image_type)
	{
        $opts = array(
            CURLOPT_RETURNTRANSFER => TRUE,  
            CURLOPT_TIMEOUT => 30,  
            CURLOPT_HEADER => 0,   
            CURLOPT_SSL_VERIFYHOST => 0,   
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_URL => self::TRAFFICENGINE_URL,
            CURLOPT_HTTPHEADER => Array("Content-type: application/json")
        );

		$request = Array();
		$request[self::OFFER_OFFER] = Array(
			self::OFFER_ZIPCODE => "$zipcode", 
			self::OFFER_VERTICAL => "$vertical", 
			self::OFFER_TYPE => self::OFFER_IMAGE, 
			self::OFFER_IMAGE => "$image_type");

		$request[self::OFFER_META] = Array(self::OFFER_META_COUNT => "$count");

		if( ! is_array($custom_data) )
		{
			$custom_data = Array("_custom_data" => $custom_data);
		}
		$request[self::OFFER_CUSTOM] = $custom_data;

		$data = json_encode($request,TRUE);
		$opts[CURLOPT_POSTFIELDS] = $data;
      
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        $result = curl_exec($ch);
        if( $result === FALSE )
        {
			error_log("CURL ERROR: " . curl_errno($ch) . " :: " . curl_error($ch));
			curl_close($ch);
			return FALSE;
        }
		curl_close($ch);
		return $result;
	}

	
	private $apikey;

	public function GetAPIKey()
	{
		return $this->apikey;
	}

	public function SetAPIKey($apikey)
	{
		$this->apikey = $apikey;
	}
}




